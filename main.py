from datetime import datetime
import os
import pygame
import pygame.font


def main():
    pygame.init()

    white = (255, 255, 255)
    width = pygame.display.Info().current_w
    height = pygame.display.Info().current_h
    SCREEN_SZ = (width, height)

    disp = pygame.display.set_mode(SCREEN_SZ, pygame.FULLSCREEN)

    f = pygame.font.SysFont("Arial", 192, 0, 0)

    time_disp = f.render(get_Time(), True, white, None)
    disp.blit(time_disp, (width/10, height/10))

    while True:

        disp.fill((0,0,0))
        time_disp = f.render(get_Time(), True, white, None)
        disp.blit(time_disp, (width/10, height/10))

        for event in pygame.event.get():
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_q:
                    pygame.quit()
                    quit()
        
        pygame.display.flip()


def get_Time():
    time = datetime.now()
    return time.strftime("%I:%M:%S %p")

if __name__ == "__main__":
    main()